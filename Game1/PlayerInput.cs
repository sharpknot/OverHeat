﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;

namespace Game1
{
    public class PlayerInput
    {
        TouchCollection touchInput;

        public PlayerInput()
        {
            touchInput = TouchPanel.GetState();
        }

        public Vector2 getTouchPosition()
        {
            if (touchInput.Count > 0)
            {
                return touchInput[0].Position;
            }
            else
            {
                return Vector2.Zero;
            } 
        }

        public Boolean hasTouch()
        {
            return touchInput.Count > 0;
        }

        public Boolean isTouchPressing()
        {
            if(touchInput.Count > 0)
            {
                return touchInput[0].State == TouchLocationState.Pressed;
            }
            else
            {
                return false;
            }
        }

    }
}