﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Game1
{
    class GameProperty
    {
        float globalMaxSpeed = 20;
        float globalMinSpeed = 15;
        ContentManager content;
        int screenWidth, screenHeight;
        Player player;
        int score;

        public GameProperty(ContentManager content, int screenWidth, int screenHeight,Player player)
        {
            this.content = content;
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;
            this.player = player;
            score = 0;
        }

        public float getGlobalMaxSpeed()
        {
            return globalMaxSpeed;
        }

        public float getGlobalMinSpeed()
        {
            return globalMinSpeed;
        }

        public ContentManager getContent()
        {
            return content;
        }

        public float getGlobalSpeed()
        {
            PlayerInput input = new PlayerInput();

            if (input.hasTouch())
            {
                return getGlobalMinSpeed();
            }
            else
            {
                return getGlobalMaxSpeed();
            }

        }

        public int getScreenWidth()
        {
            return screenWidth;
        }

        public int getScreenHeight()
        {
            return screenHeight;
        }

        public Rectangle getPlayerHitbox()
        {
            return player.getHitbox();
        }

        public void damagePlayer(int damageAmount)
        {
            player.Damage(damageAmount);
        }

        public int getPlayerHealth()
        {
            return player.health;
        }

        public int getCurrentCharge()
        {
            return player.getCharge();
        }

        public int getMaxCharge()
        {
            return player.getMaxCharge();
        }

        public void addScore(int scoreAmount)
        {
            score += scoreAmount;

        }

        public int getScore()
        {
            return score;
        }

        public void Reset()
        {
            score = 0;
        }


    }
}