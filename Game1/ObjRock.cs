﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class ObjRock : SpaceObject
    {
        ContentManager content;
        int damageAmount = 5;

        public ObjRock(int width, int height, Vector2 position, GameProperty gameProperty, Texture2D texture) : base(width, height, position, gameProperty)
        {
            content = gameProperty.getContent();

            this.texture = texture;

        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            // If player is inside the hitbox
            if (getHitbox().Intersects(gameProperty.getPlayerHitbox()))
            {
                gameProperty.damagePlayer(damageAmount);    // Damages the player for 10
                this.isAlive = false;
            }


            base.Update(gameTime, gameObjects);
        }

        
    }
}