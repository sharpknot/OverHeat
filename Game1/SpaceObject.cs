﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class SpaceObject : GameObject
    {
        protected Texture2D texture;
        protected GameProperty gameProperty;

        public SpaceObject(int width, int height, Vector2 position,GameProperty gameProperty) : base(width, height, position)
        {
            this.gameProperty = gameProperty;
        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            // Update position depending on the touch
            this.position = this.position + new Vector2(0, gameProperty.getGlobalSpeed());


            // set this object to be dead if it is out of the screen
            if(this.position.Y >= (gameProperty.getScreenHeight() + height))
            {
                isAlive = false;
            }


            base.Update(gameTime, gameObjects);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(texture, getHitbox(), null, Color.White);

            base.Draw(spriteBatch);
        }
    }
}