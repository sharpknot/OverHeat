﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Game1
{
    class ObjScore : SpaceObject
    {
        int scoreAmount = 0;

        public ObjScore(int width, int height, Vector2 position, GameProperty gameProperty, Texture2D texture, int scoreAmount) : base(width, height, position, gameProperty)
        {
            this.texture = texture;
            this.scoreAmount = scoreAmount;
        }

        public override void Update(GameTime gameTime, List<GameObject> gameObjects)
        {
            // If player is inside the hitbox
            if (getHitbox().Intersects(gameProperty.getPlayerHitbox()))
            {
                gameProperty.addScore(scoreAmount);    // adds the score
                this.isAlive = false;
            }

            base.Update(gameTime, gameObjects);
        }
    }
}