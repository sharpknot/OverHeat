﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class GameObject
    {
        Rectangle hitBox;
        protected int width, height;
        protected Vector2 position;
        protected Vector2 anchorPos;  // Anchor position (used to identify the 0,0 position of a rectangle)
        protected Texture2D texture;
        public Boolean isAlive = true;
        public Boolean isPlayer = false;
        public int health;
        

        public GameObject(int width, int height, Vector2 position)
        {
            this.width = width;
            this.height = height;
            this.position = position;

            // Centering the object
            anchorPos = Vector2.Divide(position, 2);   

            hitBox = new Rectangle((int)position.X - (width / 2), (int)position.Y - (height / 2), width, height);    // Creates a hitbox. position is in the center of the object
        }

        public virtual void Update(GameTime gameTime, List<GameObject> gameObjects)
        {

            // Updates hitbox position
            hitBox = new Rectangle((int)position.X - (width / 2), (int)position.Y - (height / 2), width, height);
            

        }

        public virtual void Draw(SpriteBatch spriteBatch) { }

        public Rectangle getHitbox()
        {
            return hitBox;
        }

        public Vector2 getAnchorPosition()
        {
            return anchorPos;
        }

        protected Boolean isColliding(GameObject gameObject)
        {
            return hitBox.Intersects(gameObject.getHitbox());
        }

        public Vector2 getPosition()
        {
            return position;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public virtual void Damage(int damageAmount)
        {

        }

        public virtual void setTexture(Texture2D objectTexture)
        {
            this.texture = objectTexture;
        }

        public virtual Texture2D getTexture()
        {
            return texture;
        }
    }
}