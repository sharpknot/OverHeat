﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class GUI
    {
        List<GUI> guiList;
        Vector2 position;
        int width, height;
        Texture2D texture;

        public GUI(Vector2 position, int width, int height)
        {
            this.position = position;
            this.width = width;
            this.height = height;

            guiList = new List<GUI>();
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach(GUI g in guiList)
            {
                g.Update(gameTime);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (GUI g in guiList)
            {
                g.Draw(spriteBatch);
            }

            if(texture != null)
            {
                spriteBatch.Draw(texture, position, new Rectangle((int)position.X, (int)position.Y, width, height), Color.White);
            }
        }

        public void setTexture(Texture2D texture)
        {
            this.texture = texture;
        }

    }
}