﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Game1
{
    class GUI_main : GUI
    {
        //ContentManager content;
        GameProperty gameProperty;
        int chargeBarWidth, chargeBarHeight, healthBarWidth, healthBarHeight;
        float barWidthRatio = 0.01f; // how wide the bar is based on screen width
        float barRatio = 10f;   // bar ratio (height:width)     

        double chargePercent;

        int barWidth, barHeight;

        Vector2 chargeBarPos, healthBarPos,chargeLogoPos,hpLogoPos;
        Texture2D chargeTexture,healthTexture, chargeLogo,hpLogo;
        Rectangle projectedChargeBar;
        GUI chargeBar;

        double chargeRatioToScreen, healthRatioToScreen;

        public GUI_main(Vector2 position, int width, int height, GameProperty gameProperty) : base(position, width, height)
        {
            this.gameProperty = gameProperty;

            chargeTexture = gameProperty.getContent().Load<Texture2D>("white");
            healthTexture = gameProperty.getContent().Load<Texture2D>("white");

            barWidth = (int)(gameProperty.getScreenWidth() * barWidthRatio);
            barHeight = gameProperty.getScreenHeight();

            chargeRatioToScreen = barHeight / gameProperty.getMaxCharge();
            healthRatioToScreen = barHeight / 100;

            chargeBarWidth = barWidth;
            chargeBarHeight = (int)(barHeight * chargePercent);

            healthBarWidth = barWidth;
            healthBarHeight = (int)(gameProperty.getPlayerHealth() * healthRatioToScreen);
            chargeBarPos = new Vector2(gameProperty.getScreenWidth() - chargeBarWidth, gameProperty.getScreenHeight() - chargeBarHeight);
            healthBarPos = new Vector2(0, 0);

            hpLogo = gameProperty.getContent().Load<Texture2D>("indicatorHP");
            chargeLogo = gameProperty.getContent().Load<Texture2D>("indicatorTemp");
            hpLogoPos = new Vector2(healthBarWidth, healthBarPos.Y);
            chargeLogoPos = new Vector2(gameProperty.getScreenWidth() - healthBarWidth - chargeLogo.Width, chargeBarPos.Y);

            
        }


        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            chargePercent = gameProperty.getCurrentCharge() / gameProperty.getMaxCharge();
            chargeBarHeight = (int)(gameProperty.getCurrentCharge() * chargeRatioToScreen);
            chargeBarPos = new Vector2(gameProperty.getScreenWidth() - chargeBarWidth, gameProperty.getScreenHeight() - chargeBarHeight);


            healthBarHeight = (int)(gameProperty.getPlayerHealth() * healthRatioToScreen);
            healthBarPos = new Vector2(0, gameProperty.getScreenHeight() - healthBarHeight);

            hpLogoPos = new Vector2(healthBarWidth, healthBarPos.Y);
            chargeLogoPos = new Vector2(gameProperty.getScreenWidth() - healthBarWidth - chargeLogo.Width, chargeBarPos.Y);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            Color chargeColor;

            if (gameProperty.getCurrentCharge() < 50)
            {
                chargeColor = Color.Green;
            }
            else if(gameProperty.getCurrentCharge() >= 50 && gameProperty.getCurrentCharge() < 75)
            {
                chargeColor = Color.Yellow;
            }
            else
            {
                chargeColor = Color.Red;
            }


            spriteBatch.Draw(chargeTexture, new Rectangle((int)chargeBarPos.X, (int)chargeBarPos.Y, chargeBarWidth, chargeBarHeight), chargeColor);

            Color healthColor;

            if(gameProperty.getPlayerHealth() > 60)
            {
                healthColor = Color.Green;
            }
            else if(gameProperty.getPlayerHealth() <= 60 && gameProperty.getPlayerHealth() > 40)
            {
                healthColor = Color.GreenYellow;
            }
            else if(gameProperty.getPlayerHealth() <= 40 && gameProperty.getPlayerHealth() > 20)
            {
                healthColor = Color.Yellow;
            }
            else
            {
                healthColor = Color.Red;
            }

            spriteBatch.Draw(healthTexture, new Rectangle(0, (int)healthBarPos.Y, healthBarWidth, healthBarHeight), healthColor);


            spriteBatch.Draw(hpLogo, hpLogoPos, Color.WhiteSmoke);
            spriteBatch.Draw(chargeLogo, chargeLogoPos, Color.WhiteSmoke);
        }
    }
}